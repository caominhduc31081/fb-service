@extends('layout.app')

@section('container')
    <div class="container">
        @foreach($posts as $post)
        <div class="col-sm-3 mt-2 float-left ">
          <div class="card">
              <img class="card-img-top" src="{{url('storage/images/'.$post['image'])}}" alt="Card image cap">
              <div class="card-body">
                  <p class="card-text">{{$post['content']}}</p>
                  <button class="btn btn-sm btn-primary"
                      onclick="$('#upPost{{$post['id']}}').submit()">
                      Post
                  </button>
                  <button class="btn btn-sm btn-danger"
                      onclick="$('#deletePost{{$post['id']}}').submit()">
                      Delete
                  </button>
                  <form id="upPost{{$post['id']}}" action="{{url()->current()}}" method="post" style="display: none;">
                      @csrf
                      <input type="text" name="id" value="{{$post['id']}}">
                      <textarea name="content" >{{$post['content']}}</textarea>
                      <input type="text" name="image" value="{{$post['image']}}">
                  </form>
                  <form id="deletePost{{$post['id']}}" action="{{url()->current().'/'.$post['id']}}" method="post" style="display: none;">
                      @csrf
                      @method('DELETE')
                  </form>
              </div>
          </div>
        </div>
        @endforeach
    </div>
@endsection
