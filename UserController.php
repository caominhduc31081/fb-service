<?php

namespace App\Http\Controllers\Crawler;

use App\Post;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Exception\GuzzleException;


class UserController extends Controller
{
    protected $_tokenUser;
    protected $_tokenPage;
    protected $_client;
    protected $_page;

    public function __construct()
    {
        $this->_tokenUser = 'EAACW5Fg5N2IBADNIx2IVMIzsXMUh0DXwQVrlZCLo9GSlWCwGZBWBcJeD3oBnMS5D2ezBOfufTTzZCzgrSrZATanWGmjQh37WH0i8mv5sNNKqflSTYP8nN9yIhpCih9vAvGyHe0136LfYQU11UdcdwAn5Hn1JNfq5cka3J43wCcqNy99ukiCE';
        $this->_tokenPage = 'EAACW5Fg5N2IBACsBZBPFH9CnaLAtYjGKBhbKU1atkN6tKAouda6fzxsk9dFZCF7tyIXrG29YwRf86R3SA9W3NIaQilToDAZCobx89WlqDEwDvoVtU7jTtPY9vFlx8oZCjI1FIOqczMvETSza8yqTaTLfJN5aCxDNoKOmXb29zDdjMzAcmUCk';
        $this->_client = new Client([
            'base_uri' => 'https://graph.facebook.com/v3.2/'
        ]);
        $this->_page = '535883206756490';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('created_at','>=',Carbon::now()->subWeek())
                    ->where('is_deleted',false)
                    ->orderBy('created_at','desc')
                    ->get()->toArray();

        return view('user.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->content);
        $response = $this->_client
                        ->post("$this->_page/photos",[
                            'query' => [
                                'access_token'  => $this->_tokenPage,
                                'message' => $request->content ?? '',
                            ],
                            'multipart' => [
                                [
                                    'name' => 'source',
                                    'contents' => fopen(storage_path('app/public/images/'.$request->image),'r')
                                ],
                            ]
                        ]);
        return $this->destroy($request->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $posts = $this->crawlByUserId($id);
        
        $this->saveCrawlerResults($posts,$id);

        return redirect('/crawler/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->update(['is_deleted'=>true]);
        $post = json_decode($post->toJson());
        unlink(storage_path('app/public/images/'.$post->image));
        return redirect('crawler/users');
    }

    public function subcrible()
    {
        $users = [
            '100003737237606',
            '100004270604603',
            '100022827962882'
        ];
        foreach($users as $id){
            $posts = $this->crawlByUserId($id);
            $this->saveCrawlerResults($posts,$id);
        }
        return redirect('crawler/users'); 
    }
    protected function crawlByUserId($id)
    {
        $response = $this->_client
                        ->get("$id/posts",[
                            'query' => [
                                'fields' => 'full_picture,message',
                                'access_token' => $this->_tokenUser,
                            ]
                        ]);
        
        $data = json_decode($response->getBody()->getContents());
        return $data->data;
    }

    protected function saveCrawlerResults($posts,$id)
    {
        $postsSaved = Post::where('object_id',$id)
                            ->get()->toArray();

        $idsSaved = array_map(function($post){
            return $post['post_id'];
        },$postsSaved);
        
        foreach($posts as $key=>$post){
            
            if(in_array($post->id,$idsSaved)) 
                continue;

            if(!isset($post->full_picture))
                continue;

            $image_link = $post->full_picture;
            $image_name = Image::make($image_link)
                                ->save(storage_path('app/public/images/'.$post->id.'.jpg'))
                                ->basename;

            Post::updateOrCreate([
                'object_type' => 'user',
                'object_id' => $id,
                'post_id' => $post->id,
                'content' => $post->message ?? '',
                'image' => $image_name ?? '',
            ]);
        }
    }
}
